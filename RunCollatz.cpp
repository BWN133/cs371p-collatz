// --------------
// RunCollatz.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Collatz.hpp"
#include <utility>
using namespace std;

// ----
// main
// ----

int main () {
    collatz_solve(cin, cout);
    return 0;
}
