// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));}


TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1, 2)), make_tuple(1, 2, 2));}
    
TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(2,3)), make_tuple(2, 3, 8));}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(5,7)), make_tuple(5, 7, 17));}


TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(1,800)), make_tuple(1, 800, 171));}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(200,2600)), make_tuple(200,2600,209));}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(435,7489)), make_tuple(435,7489,262));}



TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(399,799)), make_tuple(399,799,171));}


TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(2,999999)), make_tuple(2,999999,525));}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(799,399)), make_tuple(799,399,171));}


TEST(CollatzFixture, eval13) {
    ASSERT_EQ(collatz_eval(make_pair(998888,2)), make_tuple(998888,2,525));}

TEST(CollatzFixture, eval14) {
    ASSERT_EQ(collatz_eval(make_pair(150,748)), make_tuple(150, 748, 171));}


TEST(CollatzFixture, eval15) {
    ASSERT_EQ(collatz_eval(make_pair(101,899)), make_tuple(101, 899, 179));}

TEST(CollatzFixture, eval16) {
    int bank[27] = {
499863, 480007, 426,
822183, 100404, 509,
360450, 235842, 441,
299214, 360399, 441,
822296, 816906, 450,
640513, 656341, 416,
288431, 288857, 389,
859112, 859562, 313,
480117, 484282, 413
    };
    for(int index = 0; index < 9; index++){
            ASSERT_EQ(collatz_eval(make_pair(bank[index*3], bank[index*3 + 1])), make_tuple(bank[index*3], bank[index*3+1], bank[index*3+2]));}
    }


TEST(CollatzFixture, eval17) {
    ASSERT_EQ(collatz_eval(make_pair(822183, 100404)), make_tuple(822183, 100404, 509));}

TEST(CollatzFixture, eval18) {
    ASSERT_EQ(cycle_length(837799), 525);}


// 499863, 480007, 426
// 822183, 100404, 509
// 360450, 235842, 441
// 299214, 360399, 441
// 822296, 816906, 450
// 640513, 656341, 416
// 288431, 288857, 389
// 859112, 859562, 313
// 480117, 484282, 413
// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n1 2\n2 3\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n1 2 2\n2 3 8\n");}
